# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sairan Kikkarin <sairan(at)computer.org>, 2010.
# Sairan Kikkarin <sairan@computer.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-21 02:30+0000\n"
"PO-Revision-Date: 2011-12-08 04:01+0600\n"
"Last-Translator: Sairan Kikkarin <sairan@computer.org>\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Сайран Киккарин"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sairan@computer.org"

#: EditPage.cpp:180
#, kde-format
msgid ""
"The KDE Power Management System will now generate a set of defaults based on "
"your computer's capabilities. This will also erase all existing "
"modifications you made. Are you sure you want to continue?"
msgstr ""
"KDE-нің қуаттандыруды басқару жүйесі қазір, компьютеріңіздің мүмкіндіктері "
"негізінде, бір қатар әдетті профильдерін құрмақ. Бұл амал барлық бұрынғы "
"өзгерістеріңізді құртады. Жалғастыра берейік пе?"

#: EditPage.cpp:184
#, kde-format
msgid "Restore Default Profiles"
msgstr "Әдетті профильдеріне қайту"

#: EditPage.cpp:261
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: profileEditPage.ui:21
#, kde-format
msgid "On AC Power"
msgstr "Токқа қосылған"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: profileEditPage.ui:31
#, kde-format
msgid "On Battery"
msgstr "Батареяда"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: profileEditPage.ui:41
#, kde-format
msgid "On Low Battery"
msgstr "Қуаты аз батареяда"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Қуаттандыруны басқару қызметі жегілмеген сияқты.\n"
#~ "Бұл \"Бастау мен сөндіру\" дегенде жегіп не жоспарлап шешіледі"

#~ msgid "Power Profiles Configuration"
#~ msgstr "Қуаттандыру профильдерді баптау"

#~ msgid "A profile configurator for KDE Power Management System"
#~ msgstr "KDE-нің қуаттандыруды басқару жүйесінің профилін баптауы"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can manage KDE Power Management System's power "
#~ "profiles, by tweaking existing ones or creating new ones."
#~ msgstr ""
#~ "Бұл модулінен KDE-нің қуаттандыруды жүйесінің қуаттандыру профилін, бар "
#~ "біреуін өзгертіп не жаңасын құрып, баптауға болады"

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Жетілдірушісі"

#~ msgid "Please enter a name for the new profile:"
#~ msgstr "Жаңа профилінің атауы:"

#~ msgid "The name for the new profile"
#~ msgstr "Жаңа профилін атау"

#~ msgid "Enter here the name for the profile you are creating"
#~ msgstr "Мұнда құрылып жатқан профилінің атауын келтіріңіз"

#~ msgid "Please enter a name for this profile:"
#~ msgstr "Профилінің атауы:"

#~ msgid "Import Power Management Profiles"
#~ msgstr "Қуаттандыруды басқару профильдерін импорттау"

#~ msgid "Export Power Management Profiles"
#~ msgstr "Қуаттандыруды басқару профильдерін экспорттау"

#~ msgid ""
#~ "The current profile has not been saved.\n"
#~ "Do you want to save it?"
#~ msgstr ""
#~ "Назардағы профилі сақталмаған.\n"
#~ "Сақтауы қажет пе?"

#~ msgid "Save Profile"
#~ msgstr "Профильді сақтау"

#~ msgid "New Profile"
#~ msgstr "Жаңа профилі"

#~ msgid "Delete Profile"
#~ msgstr "Профильді өшіру"

#~ msgid "Import Profiles"
#~ msgstr "Профилін импорттау"

#~ msgid "Export Profiles"
#~ msgstr "Профилін экспорттау"

#~ msgid "Edit Profile"
#~ msgstr "Профилін өңдеу"
