# Lithuanian translations for l package.
# Copyright (C) 2011 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
#
# Andrius Štikonas <andrius@stikonas.eu>, 2011.
# Tomas Straupis <tomasstraupis@gmail.com>, 2011.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Donatas G. <dgvirtual@akl.lt>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: powerdevilprofilesconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-21 02:30+0000\n"
"PO-Revision-Date: 2021-08-21 16:43+0300\n"
"Last-Translator: Moo\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrius Štikonas, Moo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrius@stikonas.eu, <>"

#: EditPage.cpp:180
#, kde-format
msgid ""
"The KDE Power Management System will now generate a set of defaults based on "
"your computer's capabilities. This will also erase all existing "
"modifications you made. Are you sure you want to continue?"
msgstr ""
"KDE energijos valdymo sistema dabar sukurs keletą numatytųjų nustatymų pagal "
"tai, ką gali jūsų kompiuteris. Tai taipogi panaikins visus esamus jūsų "
"atliktus pakeitimus. Ar tikrai norite tęsti?"

#: EditPage.cpp:184
#, kde-format
msgid "Restore Default Profiles"
msgstr "Atkurti numatytuosius profilius"

#: EditPage.cpp:261
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Atrodo, kad energijos valdymo tarnyba neveikia."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: profileEditPage.ui:21
#, kde-format
msgid "On AC Power"
msgstr "Naudojant išorinį maitinimą"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: profileEditPage.ui:31
#, kde-format
msgid "On Battery"
msgstr "Naudojant akumuliatorių"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: profileEditPage.ui:41
#, kde-format
msgid "On Low Battery"
msgstr "Senkant akumuliatoriui"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Energijos valdymo tarnyba atrodo neveikia.\n"
#~ "Ši problema gali būti išspręsta „Paleidimo ir išjungimo“ modulyje."
