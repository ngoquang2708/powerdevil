# Translation for powerdevil.po to Euskara/Basque (eu).
# Copyright (C) 2008-2022, This file is copyright:
# This file is distributed under the same license as the powerdevil package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2008, 2009, 2010, 2011, 2014, 2017, 2018, 2019, 2020, 2021, 2022.
# marcos <marcos@euskalgnu.org>, 2010.
# Hizkuntza Politikarako Sailburuordetza <hizpol@ej-gv.es>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-19 02:03+0000\n"
"PO-Revision-Date: 2022-11-25 20:03+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.08.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xalba@ni.eus,Hizpol@ej-gv.es"

#: actions/bundled/brightnesscontrol.cpp:53 actions/bundled/dpms.cpp:75
#: actions/bundled/handlebuttonevents.cpp:57
#: actions/bundled/keyboardbrightnesscontrol.cpp:57
#, kde-format
msgctxt "Name for powerdevil shortcuts category"
msgid "Power Management"
msgstr "Energia-kudeaketa"

#: actions/bundled/brightnesscontrol.cpp:56
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Screen Brightness"
msgstr "Handitu pantailaren distira"

#: actions/bundled/brightnesscontrol.cpp:61
#, fuzzy, kde-format
#| msgctxt "@action:inmenu Global shortcut"
#| msgid "Increase Screen Brightness"
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Screen Brightness by 1%"
msgstr "Handitu pantailaren distira"

#: actions/bundled/brightnesscontrol.cpp:66
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Screen Brightness"
msgstr "Txikitu pantailaren distira"

#: actions/bundled/brightnesscontrol.cpp:71
#, fuzzy, kde-format
#| msgctxt "@action:inmenu Global shortcut"
#| msgid "Decrease Screen Brightness"
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Screen Brightness by 1%"
msgstr "Txikitu pantailaren distira"

#: actions/bundled/brightnesscontrolconfig.cpp:75
#, kde-format
msgctxt "Brightness level, label for the slider"
msgid "Level"
msgstr "Maila"

#: actions/bundled/dimdisplayconfig.cpp:64 actions/bundled/dpmsconfig.cpp:62
#: actions/bundled/runscriptconfig.cpp:82
#: actions/bundled/suspendsessionconfig.cpp:86
#, kde-format
msgid " min"
msgstr " min"

#: actions/bundled/dimdisplayconfig.cpp:74
#: actions/bundled/runscriptconfig.cpp:85
#: actions/bundled/runscriptconfig.cpp:107
#, kde-format
msgid "After"
msgstr "Honen ondoren:"

#: actions/bundled/dpms.cpp:78
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Turn Off Screen"
msgstr "Itzali pantaila"

#: actions/bundled/dpmsconfig.cpp:63
#, kde-format
msgid "Switch off after"
msgstr "Itzali honen ondoren:"

#: actions/bundled/dpmsconfig.cpp:71
#, kde-format
msgid " sec"
msgstr " seg"

#: actions/bundled/dpmsconfig.cpp:72
#, kde-format
msgid "When screen is locked, switch off after"
msgstr ""

#: actions/bundled/handlebuttonevents.cpp:62
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Suspend"
msgstr "Eseki"

#: actions/bundled/handlebuttonevents.cpp:67
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Hibernate"
msgstr "Hibernatu"

#: actions/bundled/handlebuttonevents.cpp:72
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Power Off"
msgstr "Itzali"

#: actions/bundled/handlebuttonevents.cpp:86
#, kde-format
msgctxt ""
"@action:inmenu Global shortcut, used for long presses of the power button"
msgid "Power Down"
msgstr "Erabat itzali"

#: actions/bundled/handlebuttoneventsconfig.cpp:87
#, kde-format
msgctxt "Execute action on lid close even when external monitor is connected"
msgid "Even when an external monitor is connected"
msgstr "Kanpo-monitore bat konektatuta dagoenean ere"

#: actions/bundled/handlebuttoneventsconfig.cpp:97
#, kde-format
msgid "Do nothing"
msgstr "Ez egin ezer"

#: actions/bundled/handlebuttoneventsconfig.cpp:99
#: actions/bundled/suspendsessionconfig.cpp:90
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Egin lo"

#: actions/bundled/handlebuttoneventsconfig.cpp:102
#: actions/bundled/suspendsessionconfig.cpp:93
#, kde-format
msgid "Hibernate"
msgstr "Hibernatu"

#: actions/bundled/handlebuttoneventsconfig.cpp:105
#: actions/bundled/suspendsessionconfig.cpp:96
#, kde-format
msgid "Hybrid sleep"
msgstr "Lokartze hibridoa"

#: actions/bundled/handlebuttoneventsconfig.cpp:107
#: actions/bundled/suspendsessionconfig.cpp:98
#, kde-format
msgid "Shut down"
msgstr "Itzali"

#: actions/bundled/handlebuttoneventsconfig.cpp:108
#: actions/bundled/suspendsessionconfig.cpp:99
#, kde-format
msgid "Lock screen"
msgstr "Giltzatu pantaila"

#: actions/bundled/handlebuttoneventsconfig.cpp:110
#, kde-format
msgid "Prompt log out dialog"
msgstr "Erakutsi saio-amaitzeko elkarrizketa-koadroa"

#: actions/bundled/handlebuttoneventsconfig.cpp:112
#, kde-format
msgid "Turn off screen"
msgstr "Itzali pantaila"

#: actions/bundled/handlebuttoneventsconfig.cpp:138
#, kde-format
msgid "When laptop lid closed"
msgstr "Magalekoaren tapa itxita dagoenean"

#: actions/bundled/handlebuttoneventsconfig.cpp:149
#, kde-format
msgid "When power button pressed"
msgstr "Indarraren botoia sakatzean"

#: actions/bundled/keyboardbrightnesscontrol.cpp:61
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Keyboard Brightness"
msgstr "Handitu pantailaren distira"

#: actions/bundled/keyboardbrightnesscontrol.cpp:66
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Keyboard Brightness"
msgstr "Txikitu pantailaren distira"

#: actions/bundled/keyboardbrightnesscontrol.cpp:71
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Toggle Keyboard Backlight"
msgstr "Aktibatu/Desaktibatu teklatuaren atzeko argia"

#: actions/bundled/keyboardbrightnesscontrolconfig.cpp:77
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Level"
msgstr "Maila"

#: actions/bundled/powerprofileconfig.cpp:84
#, kde-format
msgid "Leave unchanged"
msgstr "Utzi aldatzeke"

#: actions/bundled/powerprofileconfig.cpp:92
#, kde-format
msgid "Power Save"
msgstr "Energia aurrezpena"

#: actions/bundled/powerprofileconfig.cpp:93
#, kde-format
msgid "Balanced"
msgstr "Orekatua"

#: actions/bundled/powerprofileconfig.cpp:94
#, kde-format
msgid "Performance"
msgstr "Errendimendua"

#: actions/bundled/powerprofileconfig.cpp:109
#, kde-format
msgctxt "Switch to power management profile"
msgid "Switch to:"
msgstr "Aldatu honetara:"

#: actions/bundled/runscriptconfig.cpp:71
#, kde-format
msgid "Script"
msgstr "Scripta"

#: actions/bundled/runscriptconfig.cpp:83
#, kde-format
msgid "On Profile Load"
msgstr "Profila kargatzean"

#: actions/bundled/runscriptconfig.cpp:84
#, kde-format
msgid "On Profile Unload"
msgstr "Profila deskargatzean"

#: actions/bundled/runscriptconfig.cpp:96
#, kde-format
msgid "Run script"
msgstr "Exekutatu scripta"

#: actions/bundled/suspendsessionconfig.cpp:87
#, kde-format
msgid "after "
msgstr "Hau igarota:"

#: actions/bundled/suspendsessionconfig.cpp:108
#, kde-format
msgid "Automatically"
msgstr "Automatikoki"

#: actions/bundled/suspendsessionconfig.cpp:114
#, kde-format
msgid "While asleep, hibernate after a period of inactivity"
msgstr "Lo dagoela, hibernatu jarduerarik gabeko denbora tarte bat igarota"

#: backends/upower/login1suspendjob.cpp:79
#, kde-format
msgid "Unsupported suspend method"
msgstr "Esekitze-metodoa ez da onartzen"

#: powerdevilapp.cpp:62
#, kde-format
msgid "KDE Power Management System"
msgstr "KDEren energia kudeatzeko systema"

#: powerdevilapp.cpp:63
#, kde-format
msgctxt "@title"
msgid ""
"PowerDevil, an advanced, modular and lightweight power management daemon"
msgstr "PowerDevil, energia kudeatzeko daimon aurreratu, modular eta arina"

#: powerdevilapp.cpp:65
#, kde-format
msgctxt "@info:credit"
msgid "(c) 2015-2019 Kai Uwe Broulik"
msgstr "(c) 2015-2019 Kai Uwe Broulik"

#: powerdevilapp.cpp:66
#, kde-format
msgctxt "@info:credit"
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: powerdevilapp.cpp:67
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer"
msgstr "Mantentzailea"

#: powerdevilapp.cpp:69
#, kde-format
msgctxt "@info:credit"
msgid "Dario Freddi"
msgstr "Dario Freddi"

#: powerdevilapp.cpp:70
#, kde-format
msgctxt "@info:credit"
msgid "Previous maintainer"
msgstr "Aurreko mantentzailea"

#: powerdevilapp.cpp:159
#, kde-format
msgid "Replace an existing instance"
msgstr "Ordezkatu existitzen den instantzia bat"

#: powerdevilcore.cpp:404 powerdevilcore.cpp:416
#, kde-format
msgid "Activity Manager"
msgstr "Jarduera-kudeatzailea"

#: powerdevilcore.cpp:405
#, kde-format
msgid "This activity's policies prevent the system from going to sleep"
msgstr "Jarduera honen gidalerroek sistema lokartzea galarazten dute"

#: powerdevilcore.cpp:417
#, kde-format
msgid "This activity's policies prevent screen power management"
msgstr ""
"Jarduera honen gidalerroek pantailaren energia kudeatzea galarazten dute"

#: powerdevilcore.cpp:480
#, kde-format
msgid "Extra Battery Added"
msgstr "Bateria gehigarria erantsi da"

#: powerdevilcore.cpp:481 powerdevilcore.cpp:702
#, kde-format
msgid "The computer will no longer go to sleep."
msgstr "Konputagailuak ez da aurrerantzean lokartuko."

#: powerdevilcore.cpp:548
#, kde-format
msgctxt "%1 is vendor name, %2 is product name"
msgid "%1 %2"
msgstr "%1 %2"

#: powerdevilcore.cpp:551
#, kde-format
msgctxt "The battery in an external device"
msgid "Device Battery Low (%1% Remaining)"
msgstr "Gailuaren bateria eskas dago (%%1 gelditzen da)"

#: powerdevilcore.cpp:553
#, kde-format
msgctxt "Placeholder is device name"
msgid ""
"The battery in \"%1\" is running low, and the device may turn off at any "
"time. Please recharge or replace the battery."
msgstr ""
"\"%1\"(e)ko bateria husten ari da, eta gailua edozein unetan itzal daiteke. "
"Bateria zamatu edo alda ezazu."

#: powerdevilcore.cpp:559
#, kde-format
msgid "Mouse Battery Low (%1% Remaining)"
msgstr "Saguaren bateria eskas dago (% %1 gelditzen da)"

#: powerdevilcore.cpp:563
#, kde-format
msgid "Keyboard Battery Low (%1% Remaining)"
msgstr "Teklatuaren bateria eskas dago (% %1 gelditzen da)"

#: powerdevilcore.cpp:567
#, kde-format
msgid "Bluetooth Device Battery Low (%1% Remaining)"
msgstr "Bluetooth gailuaren bateria eskas dago (% %1 gelditzen zaio)"

#: powerdevilcore.cpp:569
#, kde-format
msgctxt "Placeholder is device name"
msgid ""
"The battery in Bluetooth device \"%1\" is running low, and the device may "
"turn off at any time. Please recharge or replace the battery."
msgstr ""
"\"%1\" bluetooth gailuko bateria husten ari da, eta gailua edozein unetan "
"itzal daiteke. Bateria zamatu edo aldatu beharra dago."

#: powerdevilcore.cpp:613
#, kde-format
msgid ""
"Battery running low - to continue using your computer, make sure that the "
"power adapter is plugged in and that it provides enough power."
msgstr ""
"Bateria husten ari da - zure ordenagailua erabiltzen jarraitzeko, ziurtatu "
"energia egokigailua entxufatuta dagoela eta behar adina energia hornitzen "
"duela."

#: powerdevilcore.cpp:615
#, kde-format
msgid ""
"Battery running low - to continue using your computer, plug it in or shut it "
"down and change the battery."
msgstr ""
"Zure bateria husten ari da - zure ordenagailua erabiltzen jarraitzeko, "
"entxufatu edo itzali eta bateria zamatu ezazu."

#: powerdevilcore.cpp:632
#, kde-format
msgctxt ""
"Cancel timeout that will automatically put system to sleep because of low "
"battery"
msgid "Cancel"
msgstr "Utzi"

#: powerdevilcore.cpp:644
#, kde-format
msgid "Battery level critical. Your computer will shut down in 60 seconds."
msgstr "Bateria maila larria. Zure ordenagailua 60 segundo barru itzaliko da."

#: powerdevilcore.cpp:645
#, kde-format
msgctxt ""
"@action:button Shut down without waiting for the battery critical timer"
msgid "Shut Down Now"
msgstr "Itzali orain"

#: powerdevilcore.cpp:650
#, kde-format
msgid ""
"Battery level critical. Your computer will enter hibernation mode in 60 "
"seconds."
msgstr ""
"Bateria maila larria. Zure ordenagailua 60 segundo barru hibernatuko da."

#: powerdevilcore.cpp:651
#, kde-format
msgctxt ""
"@action:button Enter hibernation mode without waiting for the battery "
"critical timer"
msgid "Hibernate Now"
msgstr "Hibernatu orain"

#: powerdevilcore.cpp:656
#, kde-format
msgid "Battery level critical. Your computer will go to sleep in 60 seconds."
msgstr ""
"Bateria maila larria. Zure ordenagailuak 60 segundo barru lokartuko da."

#: powerdevilcore.cpp:657
#, kde-format
msgctxt ""
"@action:button Suspend to ram without waiting for the battery critical timer"
msgid "Sleep Now"
msgstr "Egin lo orain"

#: powerdevilcore.cpp:662
#, kde-format
msgid "Battery level critical. Please save your work."
msgstr "Bateria maila larria. Zure lana gorde ezazu."

#: powerdevilcore.cpp:673
#, kde-format
msgid "Battery Low (%1% Remaining)"
msgstr "Bateria eskas dago (% %1 gelditzen da)"

#: powerdevilcore.cpp:677
#, kde-format
msgid "Battery Critical (%1% Remaining)"
msgstr "Bateria larri dago (% %1 gelditzen da)"

#: powerdevilcore.cpp:701
#, kde-format
msgid "AC Adapter Plugged In"
msgstr "Korronte alternoko egokigailua konektatuta"

#: powerdevilcore.cpp:704
#, kde-format
msgid "Running on AC power"
msgstr "Korronte alternoarekin ibiltzen ari da"

#: powerdevilcore.cpp:704
#, kde-format
msgid "The power adapter has been plugged in."
msgstr "Elikagailua entxufatu egin da."

#: powerdevilcore.cpp:707
#, kde-format
msgid "Running on Battery Power"
msgstr "Bateria-energiarekin ibiltzen ari da"

#: powerdevilcore.cpp:707
#, kde-format
msgid "The power adapter has been unplugged."
msgstr "Elikagailua desentxufatu egin da."

#: powerdevilcore.cpp:768
#, kde-format
msgid "Charging Complete"
msgstr "Zamatzea osatu da"

#: powerdevilcore.cpp:768
#, kde-format
msgid "Battery now fully charged."
msgstr "Bateria erabat zamatuta dago orain."

#~ msgid "Turn off"
#~ msgstr "Itzali"

#~ msgid "Turn on"
#~ msgstr "Piztu"

#~ msgid "Wi-Fi"
#~ msgstr "Wi-Fi"

#~ msgid "Mobile broadband"
#~ msgstr "Mugikorraren banda-zabala"

#~ msgid "Bluetooth"
#~ msgstr "Bluetooth"

#~ msgid "Can't open file"
#~ msgstr "Ezin da fitxategia ireki"

#, fuzzy
#~| msgid ""
#~| "No valid Power Management backend plugins are available. A new "
#~| "installation might solve this problem."
#~ msgid ""
#~ "No valid Power Management backend plugins available. A new installation "
#~ "might solve this problem."
#~ msgstr ""
#~ "Ez dago energia kudeatzeko bizkarraldeko-plugin baliozkorik erabilgarri. "
#~ "Berriz instalatuta konpon liteke arazoa."

#, fuzzy
#~| msgid ""
#~| "The profile \"%1\" has been selected, but it does not exist.\n"
#~| "Please check your PowerDevil configuration."
#~ msgid ""
#~ "Profile \"%1\" has been selected but does not exist.\n"
#~ "Please check your PowerDevil configuration."
#~ msgstr ""
#~ "\"%1\" profila hautatu da, baina ez da existitzen.\n"
#~ "Egiaztatu PowerDevil-en konfigurazioa."

#~ msgid ""
#~ "Could not connect to battery interface.\n"
#~ "Please check your system configuration"
#~ msgstr ""
#~ "Ezin da konektatu bateria-interfazera.\n"
#~ "Egiaztatu sistema-konfigurazioa"

#, fuzzy
#~| msgid ""
#~| "KDE Power Management System could not be initialized. The backend "
#~| "reported the following error: %1\n"
#~| "Please check your system configuration"
#~ msgid ""
#~ "The KDE Power Management System could not be initialized. The backend "
#~ "reported the following error: %1\n"
#~ "Please check your system configuration"
#~ msgstr ""
#~ "KDEren energia kudeatzeko sistema ezin da hasieratu. Bizkarraldeak errore "
#~ "hau jakinarazi du: %1\n"
#~ "Egiaztatu zure sistema-konfigurazioa"

#~ msgid "All pending suspend actions have been canceled."
#~ msgstr "Zain dauden esekitze-ekintza guztiak bertan behera utzi dira."

#~ msgctxt "Placeholder is device name"
#~ msgid ""
#~ "The battery in your keyboard (\"%1\") is low, and the device may turn "
#~ "itself off at any time. Please replace or charge the battery as soon as "
#~ "possible."
#~ msgstr ""
#~ "Zure teklatuaren bateria (\"%1\") eskas dago, eta gailua edozein unetan "
#~ "itzal daiteke. Bateria aldatu edo zamatu ahal bezain laster."

#~ msgctxt "Placeholder is device name"
#~ msgid ""
#~ "The battery in a connected device (\"%1\") is low, and the device may "
#~ "turn itself off at any time. Please replace or charge the battery as soon "
#~ "as possible."
#~ msgstr ""
#~ "Konektatutako gailu baten bateria (\"%1\") eskas dago, eta gailua edozein "
#~ "unetan itzal daiteke. Bateria aldatu edo zamatu ahal bezain laster."

#~ msgid ""
#~ "Your battery level is critical, the computer will be suspended in 60 "
#~ "seconds."
#~ msgstr ""
#~ "Zure bateria-maila larria da, ordenagailua eseki egingo da 60 segundo "
#~ "barru."

#~ msgid "Hybrid suspend"
#~ msgstr "Esekitze hibridoa"

#~ msgid "Suspend"
#~ msgstr "Eseki"

#~ msgctxt "@action:inmenu Global shortcut"
#~ msgid "Sleep"
#~ msgstr "Lo egin"

#~ msgid ""
#~ "Your battery capacity is %1%. This means your battery is broken and needs "
#~ "a replacement. Please contact your hardware vendor for more details."
#~ msgstr ""
#~ "Zure bateriaren ahalmena % %1 da. Horrek esan nahi du bateria hondatuta "
#~ "dagoela eta ordeztu egin behar dela. Xehetasun gehiago nahi badituzu, "
#~ "galdetu hardware-saltzaileari."

#~ msgid ""
#~ "One of your batteries (ID %2) has a capacity of %1%. This means it is "
#~ "broken and needs a replacement. Please contact your hardware vendor for "
#~ "more details."
#~ msgstr ""
#~ "Zure baterietako baten (%2 IDa duen bateriaren) ahalmena % %1 da. Horrek "
#~ "esan nahi du bateria hondatuta dagoela eta ordeztu egin behar dela. "
#~ "Xehetasun gehiago nahi badituzu, galdetu hardware-saltzaileari."

#~ msgid "Broken Battery"
#~ msgstr "Bateria hondatuta dago"

#~ msgid ""
#~ "Your battery might have been recalled by %1. Usually, when vendors recall "
#~ "the hardware, it is because of factory defects which are usually eligible "
#~ "for a free repair or substitution. Please check <a href=\"%2\">%1's "
#~ "website</a> to verify if your battery is faulted."
#~ msgstr ""
#~ "Baliteke %1(e)k zure bateria merkatutik kendu izana. Saltzaileek "
#~ "hardwarea merkatutik kentzen dutenean, fabrika-akatsak dituelako izaten "
#~ "da, eta doan konpondu edo ordeztu ohi dute. Begiratu <a href=\"%2\">"
#~ "%1(r)en webgunea</a> zure bateria akastuna den jakiteko."

#~ msgid ""
#~ "One of your batteries (ID %3) might have been recalled by %1. Usually, "
#~ "when vendors recall the hardware, it is because of factory defects which "
#~ "are usually eligible for a free repair or substitution. Please check <a "
#~ "href=\"%2\">%1's website</a> to verify if your battery is faulted."
#~ msgstr ""
#~ "Baliteke %1(e)k zure baterietako bat (%3 IDa duena) merkatutik kendu "
#~ "izana. Saltzaileek hardwarea merkatutik kentzen dutenean, fabrika-akatsak "
#~ "dituelako izaten da, eta doan konpondu edo ordeztu ohi dute. Begiratu <a "
#~ "href=\"%2\">%1(r)en webgunea</a> zure bateria akastuna den jakiteko."

#~ msgid "Check Your Battery"
#~ msgstr "Egiaztatu bateria"

#~ msgid ""
#~ "Your Power Profiles have been updated to be used with the new KDE Power "
#~ "Management System. You can tweak them or generate a new set of defaults "
#~ "from System Settings."
#~ msgstr ""
#~ "Energia-profilak eguneratu dira KDEren energia kudeatzeko sistema "
#~ "berriarekin erabiltzeko. Profilak aldatu edo profil lehenetsi berriak sor "
#~ "ditzakezu sistemaren ezarpenetan."

#~ msgid "(c) 2010 MetalWorkers Co."
#~ msgstr "(c) 2010 MetalWorkers Co."

#~ msgid ""
#~ "The profile \"%1\" tried to activate %2, a non existent action. This is "
#~ "usually due to an installation problem or to a configuration problem."
#~ msgstr ""
#~ "\"%1\" profila %2 ekintza, existitzen ez dena, aktibatzen saiatu da. Hau "
#~ "instalazio arazo baten ondorio edo konfigurazio arazo baten ondorio da."

#~ msgctxt "Global shortcut"
#~ msgid "Sleep"
#~ msgstr "Lo"

#~ msgctxt "Global shortcut"
#~ msgid "Hibernate"
#~ msgstr "Hibernatu"

#, fuzzy
#~| msgid "Lock screen"
#~ msgid "Locking Screen"
#~ msgstr "Pantaila giltzatu"

#~ msgid "The screen is being locked"
#~ msgstr "Pantaila giltzatu egingo da"

#~ msgid ""
#~ "The power adaptor has been plugged in – all pending suspend actions have "
#~ "been canceled."
#~ msgstr ""
#~ "Indar moldagailua entxufatu da - egiteke zeuden esekitzeko ekintza "
#~ "guztiakbertan behera utzi dira."

#~ msgid "Your battery has reached a low level."
#~ msgstr "Zure bateria behe mailara iritsi da."

#~ msgctxt "Name of a power profile"
#~ msgid "Aggressive powersave"
#~ msgstr "Energia aurrezpen oldarkorra"

#~ msgid "Your battery has reached the warning level."
#~ msgstr "Zure bateria arrisku mailara iritsi da."

#~ msgid "Disable effects"
#~ msgstr "Ezgaitu efektuak"

#~ msgid "When sleep button pressed"
#~ msgstr "Lotarako botoia sakatzerakoan"

#~ msgid ""
#~ "The application %1 is inhibiting suspension for the following reason:\n"
#~ "%2"
#~ msgstr ""
#~ "%1 aplikazioa esekitzea eragozten ari da ondoko arrazoiagatik:\n"
#~ "%2"

#~ msgid "A Power Management tool for KDE4"
#~ msgstr "KDE4-an indarra kudeatzeko tresna"

#~ msgid "(c) 2008 Dario Freddi"
#~ msgstr "(c) 2008 Dario Freddi"

#~ msgid ""
#~ "Your battery level is critical, the computer will be halted in 1 second."
#~ msgid_plural ""
#~ "Your battery level is critical, the computer will be halted in %1 seconds."
#~ msgstr[0] ""
#~ "Zure bateria maila larria da, konputagailua segundu 1 barru geldituko da."
#~ msgstr[1] ""
#~ "Zure bateria maila larria da, konputagailua %1 segundu barru geldituko da."

#~ msgid ""
#~ "Your battery level is critical, the computer will be suspended to disk in "
#~ "1 second."
#~ msgid_plural ""
#~ "Your battery level is critical, the computer will be suspended to disk in "
#~ "%1 seconds."
#~ msgstr[0] ""
#~ "Zure bateria maila larri da, konputagailua diskora esekiko da segundu 1 "
#~ "barru."
#~ msgstr[1] ""
#~ "Zure bateria maila larri da, konputagailua diskora esekiko da %1 segundu "
#~ "barru."

#~ msgid ""
#~ "Your battery level is critical, the computer will be suspended to RAM in "
#~ "1 second."
#~ msgid_plural ""
#~ "Your battery level is critical, the computer will be suspended to RAM in "
#~ "%1 seconds."
#~ msgstr[0] ""
#~ "Zure bateria maila larria da, konputagailua RAM-era esekiko da segundu 1 "
#~ "barru."
#~ msgstr[1] ""
#~ "Zure bateria maila larria da, konputagailua RAM-era esekiko da %1 segundu "
#~ "barru."

#~ msgid ""
#~ "Your battery level is critical, the computer will be put into standby in "
#~ "1 second."
#~ msgid_plural ""
#~ "Your battery level is critical, the computer will be put into standby in "
#~ "%1 seconds."
#~ msgstr[0] ""
#~ "Zure bateria maila larria da, konputagailua egonean jarriko da segundu 1 "
#~ "barru."
#~ msgstr[1] ""
#~ "Zure bateria maila larria da, konputagailua egonean jarriko da %1 segundu "
#~ "barru."

#~ msgid "The computer will be suspended to disk in 1 second."
#~ msgid_plural "The computer will be suspended to disk in %1 seconds."
#~ msgstr[0] "Konputagailua diskora esekiko da segundu 1 barru."
#~ msgstr[1] "Konputagailua diskora esekiko da %1 segundu barru."

#~ msgid "The computer will be suspended to RAM in 1 second."
#~ msgid_plural "The computer will be suspended to RAM in %1 seconds."
#~ msgstr[0] "Konputagailua RAMera esekiko da segundu 1 barru."
#~ msgstr[1] "Konputagailua RAMera esekiko da %1 segundu barru."

#~ msgid "The computer will be put into standby in 1 second."
#~ msgid_plural "The computer will be put into standby in %1 seconds."
#~ msgstr[0] "Konputagailua egonean jarriko da segundu 1 barru."
#~ msgstr[1] "Konputagailua egonean jarriko da %1 segundu barru."

#~ msgid "There was an error while suspending:"
#~ msgstr "Errorea egon da esekitzerakoan:"

#~ msgid "Profile changed to \"%1\""
#~ msgstr "Profila \"%1\"-era aldatu da"

#~ msgid "Suspend to Disk"
#~ msgstr "Diskora eseki"

#~ msgid "Standby"
#~ msgstr "Egonean"

#~ msgid "General Settings"
#~ msgstr "Ezarpen orokorrak"

#~ msgid "Edit Profiles"
#~ msgstr "Profilak editatu"

#~ msgid "Capabilities"
#~ msgstr "Gaitasunak"

#~ msgid "PowerDevil Configuration"
#~ msgstr "PowerDevil konfigurazioa"

#~ msgid "A configurator for PowerDevil"
#~ msgstr "PowerDevil-entzako konfiguratzaile bat"

#~ msgid "(c), 2008 Dario Freddi"
#~ msgstr "(c), 2008 Dario Freddi"

#~ msgid ""
#~ "From this module, you can configure the Daemon, create and edit "
#~ "powersaving profiles, and see your system's capabilities."
#~ msgstr ""
#~ "Modulu honetatik, deabrua konfiguratu dezakezu, indarra aurrezteko "
#~ "profilak sortu eta editatu, eta zure sistemaren gaitasunak ikusi."

#~ msgid ""
#~ "<h1>PowerDevil configuration</h1> <p>This module lets you configure "
#~ "PowerDevil. PowerDevil is a daemon (so it runs in background) that is "
#~ "started upon KDE startup.</p> <p>PowerDevil has 2 levels of "
#~ "configuration: a general one, that is always applied, and a profile-based "
#~ "one, that lets you configure a specific behavior in every situation. You "
#~ "can also have a look at your system capabilities in the last tab. To get "
#~ "you started, first configure the options in the first 2 tabs. Then switch "
#~ "to the fourth one, and create/edit your profiles. Last but not least, "
#~ "assign your profiles in the third Tab. You do not have to restart "
#~ "PowerDevil, just click \"Apply\", and you are done.</p>"
#~ msgstr ""
#~ "<h1>PowerDevil konfiguratzeko</h1> <p>Modulo honek PowerDevil "
#~ "konfiguratzen lagunduko dizu. PowerDevil KDE abiatzen denean hasten den "
#~ "deabru bat da (beraz atzealdean exekutatzen da).</p><p> PowerDevil-ek "
#~ "konfiguratzeko 2 maila dauzka:  orokorra lehenengoa, beti aplikatzen "
#~ "dena, eta profiletan oinarritua bigarrena, egoera bakoitzerako jokabide "
#~ "zehatza konfiguratzen uzten dizuna. Azken fitxan sistemaren gaitasuna "
#~ "ikus dezakezu. Hasteko, konfiguratu lehen 2 fitxetako aukerak. Ondoren jo "
#~ "laugarrenera eta zure profilak sortu/editatu. Amaitzeko, esleitu zure "
#~ "profilak hirugarren fitxan. Ez duzu PowerDevil berrabiatu behar, klikatu "
#~ "\"Aplikatu\", eta egin duzu.</p>"

#~ msgid ""
#~ "Another power manager has been detected. PowerDevil will not start if "
#~ "other power managers are active. If you want to use PowerDevil as your "
#~ "primary power manager, please remove the existing one and restart the "
#~ "PowerDevil service."
#~ msgstr ""
#~ "Beste indar kudeatzaile bat detektatu da. PowerDevil ez da abiatuko beste "
#~ "indar kudeatzaile bat jardunean badago. PowerDevil zure indar kudeatzaile "
#~ "nagusi bezala erabili nahi baduzu, mesedez ezabatu aurrez dagoena eta "
#~ "PowerDevil zerbitzua berrabiatu."

#~ msgid ""
#~ "It seems powersaved is running on this system. PowerDevil will not start "
#~ "if other power managers are active. If you want to use PowerDevil as your "
#~ "primary power manager, please stop powersaved and restart the PowerDevil "
#~ "service."
#~ msgstr ""
#~ "Zure sisteman powersaved exekutatzen ari dela dirudi. PowerDevil ez da "
#~ "abiatuko beste indar kudeatzaile bat jardunean badago. PowerDevil zure "
#~ "indar kudeatzaile nagusi bezala erabili nahi baduzu, mesedez gelditu "
#~ "powersaved eta berrabiatu PowerDevil zerbitzua."

#~ msgid ""
#~ "PowerDevil seems not to be started. Either you have its service turned "
#~ "off, or there is a problem in D-Bus."
#~ msgstr ""
#~ "PowerDevil ez dela abiatu dirudi. Bere zerbitzua itzalduta daukazu, edo "
#~ "DBus-ekin arazo bat dago."

#, fuzzy
#~| msgid "PowerDevil Configuration"
#~ msgid "Power Profiles Configuration"
#~ msgstr "PowerDevil konfigurazioa"

#, fuzzy
#~| msgid "A configurator for PowerDevil"
#~ msgid "A profile configurator for KDE Power Management System"
#~ msgstr "PowerDevil-entzako konfiguratzaile bat"

#, fuzzy
#~| msgid "(c), 2008 Dario Freddi"
#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2008 Dario Freddi"

#~ msgid "Please enter a name for the new profile:"
#~ msgstr "Mesedez sartu profil berriarentzako izen bat:"

#~ msgid "The name for the new profile"
#~ msgstr "Profil berriarentzako izena"

#~ msgid "Enter here the name for the profile you are creating"
#~ msgstr "Sartu hemen sortzen ari zaren profilarentzako izena"

#~ msgid "Please enter a name for this profile:"
#~ msgstr "Mesedez sartu profil honentzako izen bat:"

#~ msgid "Import PowerDevil Profiles"
#~ msgstr "Inportatu PowerDevil profilak"

#~ msgid "Export PowerDevil Profiles"
#~ msgstr "Esportatu PowerDevil profilak"

#~ msgid ""
#~ "The current profile has not been saved.\n"
#~ "Do you want to save it?"
#~ msgstr ""
#~ "Uneko profila ez da gorde.\n"
#~ "Gorde nahi duzu?"

#~ msgid "Save Profile"
#~ msgstr "Gorde profila"

#~ msgctxt "None"
#~ msgid "No methods found"
#~ msgstr "Ez da metodorik aurkitu"

#~ msgid ""
#~ "ConsoleKit was not found active on your PC, or PowerDevil cannot contact "
#~ "it. ConsoleKit lets PowerDevil detect whether the current session is "
#~ "active, which is useful if you have more than one user logged into your "
#~ "system at any one time."
#~ msgstr ""
#~ "ConsoleKit ez da zure PC-an jardunean aurkitu, edo PowerDevil-ek ezin du "
#~ "kontaktatu. ConsoleKit-ek PowerDevil-i uneko saioa jardunean dagoen "
#~ "jakiten laguntzen dio, erabilgarria da zure sisteman aldi berean "
#~ "erabiltzaile batek baino gehiagok saioa hasi badu."

#~ msgid "No issues found with your configuration."
#~ msgstr "Ez da arazorik aurkitu zure konfigurazioan."

#~ msgid "New Profile"
#~ msgstr "Profil berria"

#~ msgid "Delete Profile"
#~ msgstr "Profila ezabatu"

#~ msgid "Import Profiles"
#~ msgstr "Profilak inportatu"

#~ msgid "Export Profiles"
#~ msgstr "Profilak esportatu"

#~ msgid "Settings and Profile"
#~ msgstr "Ezarpenak eta profila"

#~ msgid "Lock screen on resume"
#~ msgstr "Pantaila giltzatu berrekitean"

#~ msgid "Locks screen when waking up from suspension"
#~ msgstr "Pantaila giltzatu eseki egoeratik esnatzean"

#~ msgid "You will be asked for a password when resuming from sleep state"
#~ msgstr "Pasahitza eskatuko zaizu lo egoeratik berrekitean"

#~ msgid "Configure Notifications..."
#~ msgstr "Jakinarazpenak konfiguratu..."

#, fuzzy
#~| msgid "When AC Adaptor is plugged in"
#~ msgid "When AC Adaptor is unplugged"
#~ msgstr "AC moldagailua entxufatzen denean"

#~ msgid "When battery is at low level"
#~ msgstr "Bateria maila baxuan dagoenean"

#~ msgid "When battery is at warning level"
#~ msgstr "Bateria abisu mailan dagoenean"

#~ msgid "Advanced Battery Settings"
#~ msgstr "Bateria ezarpen aurreratuak"

#~ msgid "When battery remaining is critical"
#~ msgstr "Bateriaren zama larria denean"

#~ msgid "Battery Levels"
#~ msgstr "Bateriak mailak"

#~ msgid "Battery is at low level at"
#~ msgstr "Bateria maila baxuan dago"

#~ msgid "Low battery level"
#~ msgstr "Bateria maila baxua"

#~ msgid "Battery will be considered low when it reaches this level"
#~ msgstr "Bateria baxu dagoela joko da maila honetara iristen denean"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Battery is at warning level at"
#~ msgstr "Bateria abisu mailan dago"

#~ msgid "Warning battery level"
#~ msgstr "Bateriaren abisu maila"

#~ msgid ""
#~ "Battery will be considered at warning level when it reaches this level"
#~ msgstr "Bateria abisu mailan egongo da maila honetara iristen denean"

#~ msgid "Battery is at critical level at"
#~ msgstr "Bateria maila larrian dago"

#~ msgid "Battery will be considered critical when it reaches this level"
#~ msgstr "Bateriaren maila larria joko da maila honetara iristen denean"

#~ msgid "System Capabilities"
#~ msgstr "Sistemaren gaitasunak"

#~ msgid "Number of CPUs"
#~ msgstr "PUZ kopurua"

#~ msgid "Number of Batteries"
#~ msgstr "Bateria kopurua"

#~ msgid "Supported suspend methods"
#~ msgstr "Onartutako esekitzeko metodoak"

#~ msgid "Support for DPMS"
#~ msgstr "DPMS-ren euskarria"

#~ msgid ""
#~ "If this is enabled, PowerDevil will be able to configure power management "
#~ "for your monitor"
#~ msgstr ""
#~ "Hau gaituta badago, PowerDevil zure monitorearen indar kudeaketa "
#~ "konfiguratzeko gauza izango da "

#~ msgid "DPMS Support"
#~ msgstr "DPMS euskarria"

#~ msgid "Checks if ConsoleKit is active on your system"
#~ msgstr "Zure sisteman ConsoleKit jardunean dagoen egiaztaten du"

#~ msgid ""
#~ "ConsoleKit lets PowerDevil detect whether the current session is active, "
#~ "which is useful if you have more than one user logged into your system at "
#~ "any one time."
#~ msgstr ""
#~ "ConsoleKit-ek PowerDevil-i uneko saioa jardunean dagoen jakiten laguntzen "
#~ "dio, erabilgarria da zure sisteman aldi berean erabiltzaile batek baino "
#~ "gehiagok saioa hasi badu."

#~ msgid "ConsoleKit Runtime Support"
#~ msgstr "ConsoleKit-en exekuzio garaiko euskarria"

#~ msgid "Status"
#~ msgstr "Egoera"

#~ msgid "PowerDevil error"
#~ msgstr "PowerDevil errorea"

#~ msgid ""
#~ "The configuration module can not be started, since there seems to be a "
#~ "problem with the PowerDevil Daemon. Read below for more details"
#~ msgstr ""
#~ "Konfiguratzeko modulua ezin da abiatu, PowerDevil deabruarekin arazo bat "
#~ "dagoela dirudielako. Irakurri azpikoa xehetasun gehiago izateko"

#, fuzzy
#~| msgid "Edit Profiles"
#~ msgid "Editing Profile"
#~ msgstr "Profilak editatu"

#~ msgid "Profile Assignment"
#~ msgstr "Profil esleipena"

#~ msgid ""
#~ "<b>There are some issues in your configuration. Please check the "
#~ "Capabilities page for more details.</b>"
#~ msgstr ""
#~ "<b>Arazo batzuk daude zure konfigurazioan. Mesedez egiaztatu gaitasun "
#~ "orria xehetasun gehigo jasotzeko.</b>"

#~ msgid "Let PowerDevil manage screen powersaving"
#~ msgstr "Utzi PowerDevil-ek pantailaren indar aurrezpena kudeatu dezan"

#~ msgid "Before doing a suspend action, wait"
#~ msgstr "Esekitzeko ekinta bat egin aurretik, itxoin"

#~ msgid "Do not wait"
#~ msgstr "Ez itxoin"

#~ msgid "Do Nothing"
#~ msgstr "Ez egin ezer"

#~ msgid "Learn more about the Energy Star program"
#~ msgstr "Ikasi gehiago Energy Star programari buruz"

#~ msgid "Actions"
#~ msgstr "Ekintzak"

#~ msgid ""
#~ "To prevent data loss or other damage, you can have the system suspend or "
#~ "hibernate, so you do not run accidentally out of battery power. Configure "
#~ "the number of minutes below which the machine will run the configured "
#~ "action."
#~ msgstr ""
#~ "Datu galera edo beste kalte batzuk saihesteko, sistema eseki edo "
#~ "hibernatu dezakezu, horrela ez zara bateria indarrik gabe geldituko. "
#~ "Konfiguratu azpian makinak konfiguratutako ekintza exekutatuko duen "
#~ "minutu kopurua."

#~ msgid "When the system is idle for more than"
#~ msgstr "Sistema adierazitakoa baino denbora gehiagoz nagi egotean"

#~ msgid "This action will be performed when the laptop lid gets closed"
#~ msgstr "Ekintza hau egingo da magalekoaren tapa ixten denean"

#~ msgid "Screen"
#~ msgstr "Pantaila"

#~ msgid ""
#~ "With this slider you can set the brightness when the system is plugged "
#~ "into the socket outlet"
#~ msgstr ""
#~ "Labaingarri honekin sistema korronte hartunean entxufatuta dagoenean "
#~ "izango duen distira ezarri dezakezu."

#~ msgid "Brightness:"
#~ msgstr "Distira:"

#~ msgid "Activate automatic dimming"
#~ msgstr "Aktibatu iluntze automatikoa"

#~ msgid "Automatically dims the display when the system is idle."
#~ msgstr "Bistaratzea automatitakoki iluntzen du sistema nagi dagoenean."

#~ msgid "Dim display when idle for more than"
#~ msgstr "Bistaratzea ilundu adierazitako denbora baino gehiagoz nagi egotean"

#~ msgctxt "Minutes"
#~ msgid " min"
#~ msgstr " min"

#~ msgid "&Enable display power management"
#~ msgstr "&Gaitu bistaratzekoaren indar kudeaketa"

#~ msgid ""
#~ "Choose the period of inactivity after which the display should enter "
#~ "\"standby\" mode. This is the first level of power saving."
#~ msgstr ""
#~ "Aukeratu jardunik gabe zenbat denbora egon behar duen \"egonean\" modura "
#~ "igaroteko. Indar aurrezpenaren lehenengo maila da hau."

#~ msgid "&Standby after"
#~ msgstr "&Egonean ... ondoren"

#~ msgid "S&uspend after"
#~ msgstr "E&seki ... ondoren"

#~ msgid "CPU and System"
#~ msgstr "PUZ eta sistema"

#~ msgid "Enable system power saving"
#~ msgstr "Gaitu sistemaren indar aurrezpena"

#~ msgid "When loading profile execute:"
#~ msgstr "Profila kargatzerakoan, exekutatu:"
